<?php

namespace App\Controller;

use App\Entity\Person;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Annotations as OA;
use Nelmio\ApiDocBundle\Annotation\Model;

/**
 * Class PeopleController
 * @package App\Controller
 * @Route("/api", name="api_")
 */
class PeopleController extends AbstractFOSRestController
{
    /**
     * @return Response
     *
     * @Rest\Get("/people", name="people")
     *
     * @OA\Response(
     *     response=200,
     *     description="Returns all people registered in the database.",
     *     @OA\JsonContent(
     *        type="array",
     *        @OA\Items(ref=@Model(type=Person::class, groups={"full"}))
     *     )
     * )
     * @OA\Response(
     *     response=204,
     *     description="Return HTTP code 204 when any data found."
     * )
     * @OA\Tag(name="people")
     */
    public function findAll(): Response
    {
        try {
            $result = $this->getDoctrine()->getRepository(Person::class)->findAll();

            return $this->handleView($this->view($result));
        } catch (\Throwable $exception) {
            return $this->handleView($this->view(
                ['error' => 'An unexpected error has occurred.'],
                Response::HTTP_INTERNAL_SERVER_ERROR)
            );
        }
    }

    /**
     * @param int $id
     * @return Response
     *
     * @Rest\Get("/person/{id}", name="person")
     *
     * @OA\Parameter(
     *     name="id",
     *     in="path",
     *     description="The person ID"
     * )
     * @OA\Response(
     *     response=200,
     *     description="Returns the person data registered in the database.",
     *     @OA\JsonContent(
     *        type="object"
     *     )
     * )
     * @OA\Response(
     *     response=204,
     *     description="Return HTTP code 204 when any data found."
     * )
     *
     * @OA\Tag(name="people")
     */
    public function find(int $id): Response
    {
        try {
            $result = $this->getDoctrine()->getRepository(Person::class)->find($id);

            return $this->handleView($this->view($result));
        }  catch (\Throwable $exception) {
            return $this->handleView($this->view(
                ['error' => 'An unexpected error has occurred.'],
                Response::HTTP_INTERNAL_SERVER_ERROR)
            );
        }
    }
}
