<?php

namespace App\Controller;

use App\Form\UploadType;
use App\Service\UploadService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * Class UploadController
 * @package App\Controller
 */
class UploadController extends AbstractController
{
    /**
     * @var UploadService
     */
    private $uploadService;

    /**
     * UploadController constructor.
     * @param UploadService $uploadService
     */
    public function __construct(UploadService $uploadService)
    {
        $this->uploadService = $uploadService;
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @Route("/upload", name="upload", methods={"GET", "POST"})
     */
    public function upload(Request $request)
    {
        try {
            $form = $this->createForm(UploadType::class);

            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $peopleFiles = $form->get('peopleXml')->getData();
                $shipOrdersFiles = $form->get('shipOrdersXml')->getData();

                if ($peopleFiles && $shipOrdersFiles) {
                    $this->uploadService->process([
                        'people' => $peopleFiles,
                        'shipOrders' => $shipOrdersFiles,
                    ]);
                }

                $this->addFlash('success', 'SUCCESS: Files uploaded successfully!');
            }
        } catch (\Throwable $exception) {
            $this->addFlash('danger', 'ERROR: An unexpected error has occurred.');
        }

        return $this->render('upload/index.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
