<?php

namespace App\Controller;

use App\Entity\ShipOrder;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use OpenApi\Annotations as OA;
use Nelmio\ApiDocBundle\Annotation\Model;

/**
 * Class ShipOrderController
 * @package App\Controller
 *
 * @Route("/api", name="api_")
 */
final class ShipOrderController extends AbstractFOSRestController
{
    /**
     * @return Response
     *
     * @Rest\Get("/ship-orders")
     *
     * @OA\Response(
     *     response=200,
     *     description="Returns all ship orders registered in the database.",
     *     @OA\JsonContent(
     *        type="array",
     *        @OA\Items(ref=@Model(type=ShipOrder::class, groups={"full"}))
     *     )
     * )
     * @OA\Response(
     *     response=204,
     *     description="Return HTTP code 204 when any data found."
     * )
     * @OA\Tag(name="ship-orders")
     */
    public function findAll(): Response
    {
        try {
            $result = $this->getDoctrine()->getRepository(ShipOrder::class)->findAll();

            return $this->handleView($this->view($result));
        } catch (\Throwable $exception) {
            return $this->handleView($this->view(
                ['error' => 'An unexpected error has occurred.'],
                Response::HTTP_INTERNAL_SERVER_ERROR
            ));
        }
    }

    /**
     * @param int $id
     * @return Response
     *
     * @Rest\Get("/ship-order/{id}")
     *
     * @OA\Parameter(
     *     name="id",
     *     in="path",
     *     description="The Ship Order ID"
     * )
     *
     * @OA\Response(
     *     response=200,
     *     description="Returns the Ship Order data registered in the database.",
     *     @OA\JsonContent(
     *        type="object"
     *     )
     * )
     *
     * @OA\Response(
     *     response=204,
     *     description="Return HTTP code 204 when any data found."
     * )
     *
     * @OA\Tag(name="ship-orders")
     */
    public function find(int $id): Response
    {
        try {
            $result = $this->getDoctrine()->getRepository(ShipOrder::class)->find($id);

            return $this->handleView($this->view($result));
        } catch (\Throwable $exception) {
            return $this->handleView($this->view(
                ['error' => 'An unexpected error has occurred.'],
                Response::HTTP_INTERNAL_SERVER_ERROR
            ));
        }
    }
}
