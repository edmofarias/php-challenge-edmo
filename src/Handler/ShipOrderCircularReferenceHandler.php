<?php

namespace App\Handler;

/**
 * Class ShipOrderCircularReferenceHandler
 * @package App\Handler
 */
class ShipOrderCircularReferenceHandler
{
    /**
     * @param $object
     * @return mixed
     */
    public function __invoke($object) {
        return $object->getId();
    }
}