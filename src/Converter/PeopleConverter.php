<?php

namespace App\Converter;

use App\Entity\Person;

/**
 * Class PeopleConverter
 * @package App\Converter
 */
class PeopleConverter
{
    /**
     * @param \SimpleXMLElement $person
     * @return Person
     */
    public function convertXmlToObject(\SimpleXMLElement $person): Person
    {
        return new Person(
            strval($person->personname),
            (array) $person->phones->phone,
            intval($person->personid)
        );
    }
}