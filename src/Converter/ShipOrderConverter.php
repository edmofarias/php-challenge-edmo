<?php

namespace App\Converter;

use App\Entity\ShipOrder;
use App\Repository\PersonRepository;

/**
 * Class ShipOrderConverter
 * @package App\Converter
 */
class ShipOrderConverter
{
    /**
     * @var PersonRepository
     */
    private $personRepository;

    /**
     * ShipOrderConverter constructor.
     * @param PersonRepository $personRepository
     */
    public function __construct(PersonRepository $personRepository)
    {
        $this->personRepository = $personRepository;
    }

    /**
     * @param \SimpleXMLElement $shipOrder
     * @return ShipOrder
     */
    public function convertXmlToObject(\SimpleXMLElement $shipOrder): ShipOrder
    {
        $items = (array) $shipOrder->items;

        if ($items['item'] instanceof \SimpleXMLElement) {
            $itemsArray[] = $items['item'];
        } else {
            $itemsArray = $items['item'];
        }

        $personEntity = $this->personRepository->find(intval($shipOrder->orderperson));

        return new ShipOrder(
            $personEntity,
            (array) $shipOrder->shipto,
            $itemsArray,
            intval($shipOrder->orderid)
        );
    }
}