<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="shiporder")
 */
class ShipOrder
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $id;

    /**
     * @var Person
     *
     * @ORM\ManyToOne(targetEntity="Person")
     * @ORM\JoinColumn(name="id_person", referencedColumnName="id", nullable=false)
     */
    private $person;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="Item", mappedBy="shipOrder", cascade={"persist"})
     */
    private $items;

    /**
     * @var ShipTo
     *
     * @ORM\OneToOne(targetEntity="ShipTo", mappedBy="shipOrder", cascade={"persist"})
     */
    private $shipTo;

    /**
     * @param Person $person
     * @param array $shipTo
     * @param Item[] $items
     * @param int $id
     */
    public function __construct(
        Person $person,
        array $shipTo,
        array $items,
        int $id = 0
    ) {
        $this->id = $id;
        $this->person = $person;

        $this->shipTo = new ShipTo(
            $this,
            $shipTo['name'],
            $shipTo['address'],
            $shipTo['city'],
            $shipTo['country']
        );

        $this->items = new ArrayCollection();
        foreach ($items as $item) {
            $item = (array) $item;

            $this->items->add(
                new Item($this, $item['title'], $item['note'], $item['quantity'], $item['price'])
            );
        }
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return Person
     */
    public function getPerson(): Person
    {
        return $this->person;
    }

    /**
     * @return Collection
     */
    public function getItems(): Collection
    {
        return $this->items;
    }

    /**
     * @return ShipTo
     */
    public function getShipTo(): ShipTo
    {
        return $this->shipTo;
    }
}
