<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="item")
 */
class Item
{
    /**
     * @var int
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var ShipOrder
     * @ORM\ManyToOne(targetEntity="ShipOrder")
     * @ORM\JoinColumn(name="id_shiporder", referencedColumnName="id", nullable=false)
     */
    private $shipOrder;

    /**
     * @var string
     * @ORM\Column(type="string", length=50)
     * @Assert\NotBlank()
     */
    private $title;

    /**
     * @var string
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     */
    private $note;

    /**
     * @var int
     * @ORM\Column(type="integer")
     * @Assert\NotBlank()
     */
    private $quantity;

    /**
     * @var float
     * @ORM\Column(type="decimal", precision=7, scale=2))
     * @Assert\NotBlank()
     */
    private $price;

    /**
     * Item constructor.
     * @param ShipOrder $shipOrder
     * @param string $title
     * @param string $note
     * @param int $quantity
     * @param float $price
     */
    public function __construct(ShipOrder $shipOrder, string $title, string $note, int $quantity, float $price)
    {
        $this->shipOrder = $shipOrder;
        $this->title = $title;
        $this->note = $note;
        $this->quantity = $quantity;
        $this->price = $price;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return ShipOrder
     */
    public function getShipOrder(): ShipOrder
    {
        return $this->shipOrder;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getNote(): string
    {
        return $this->note;
    }

    /**
     * @return int
     */
    public function getQuantity(): int
    {
        return $this->quantity;
    }

    /**
     * @return float
     */
    public function getPrice(): float
    {
        return $this->price;
    }
}
