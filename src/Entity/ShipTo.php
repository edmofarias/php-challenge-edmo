<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="shipto")
 */
class ShipTo
{
    /**
     * @var int
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var ShipOrder
     *
     * @ORM\OneToOne(targetEntity="ShipOrder", inversedBy="shipTo")
     * @ORM\JoinColumn(name="id_shiporder", referencedColumnName="id", nullable=false)
     */
    private $shipOrder;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=50)
     * @Assert\NotBlank()
     */
    private $name;

    /**
     * @var string
     * @ORM\Column(type="string", length=50)
     * @Assert\NotBlank()
     */
    private $address;

    /**
     * @var string
     * @ORM\Column(type="string", length=50)
     * @Assert\NotBlank()
     */
    private $city;

    /**
     * @var string
     * @ORM\Column(type="string", length=30)
     * @Assert\NotBlank()
     */
    private $country;

    /**
     * ShipTo constructor.
     * @param ShipOrder $shipOrder
     * @param string $name
     * @param string $address
     * @param string $city
     * @param string $country
     */
    public function __construct(ShipOrder $shipOrder, string $name, string $address, string $city, string $country)
    {
        $this->shipOrder = $shipOrder;
        $this->name = $name;
        $this->address = $address;
        $this->city = $city;
        $this->country = $country;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return ShipOrder
     */
    public function getShipOrder(): ShipOrder
    {
        return $this->shipOrder;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getAddress(): string
    {
        return $this->address;
    }

    /**
     * @return string
     */
    public function getCity(): string
    {
        return $this->city;
    }

    /**
     * @return string
     */
    public function getCountry(): string
    {
        return $this->country;
    }
}
