<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\All;
use Symfony\Component\Validator\Constraints\File;

/**
 * Class UploadType
 * @package App\Form
 */
class UploadType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->setMethod('POST')
            ->add('peopleXml', FileType::class, [
                'label' => 'Choose all People xml files:',
                'mapped' => false,
                'multiple'=> true,
                'constraints' => [
                    new All([
                        new File([
                            'maxSize' => '5M',
                            'mimeTypes' => ['text/xml'],
                            'mimeTypesMessage' => 'Please upload a valid XML',
                        ])
                    ])
                ],
                'attr' => [
                    'accept' => '.xml'
                ],
            ])
            ->add('shipOrdersXml', FileType::class, [
                'label' => 'Choose all Ship Orders xml files:',
                'mapped' => false,
                'multiple'=> true,
                'constraints' => [
                    new All([
                        new File([
                            'maxSize' => '5M',
                            'mimeTypes' => ['text/xml'],
                            'mimeTypesMessage' => 'Please upload a valid XML',
                        ])
                    ])
                ],
                'attr' => [
                    'accept' => '.xml'
                ],
            ])
            ->add('save', SubmitType::class, [
                'label' => 'Process xmls files'
            ])
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            // Configure your form options here
        ]);
    }
}
