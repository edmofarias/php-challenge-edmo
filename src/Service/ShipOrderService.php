<?php

namespace App\Service;

use App\Converter\ShipOrderConverter;
use Doctrine\ORM\EntityManagerInterface;
use Spatie\Async\Pool;

/**
 * Class ShipOrderService
 * @package App\Service
 */
class ShipOrderService
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var ShipOrderConverter
     */
    private $shipOrderConverter;

    /**
     * ShipOrderService constructor.
     * @param EntityManagerInterface $entityManager
     * @param ShipOrderConverter $shipOrderConverter
     */
    public function __construct(EntityManagerInterface $entityManager, ShipOrderConverter $shipOrderConverter)
    {
        $this->entityManager = $entityManager;
        $this->shipOrderConverter = $shipOrderConverter;
    }

    /**
     * @param string $shipOrderXml
     */
    public function persist(string $shipOrderXml)
    {
        $xmlObject = simplexml_load_string($shipOrderXml);

        $pool = Pool::create();

        foreach ($xmlObject->children() as $shipOrder) {
            $pool[] = async(function () use ($shipOrder) {
                $shipOrderEntity = $this->shipOrderConverter->convertXmlToObject($shipOrder);
                $this->entityManager->persist($shipOrderEntity);
            });
        }

        $pool->wait();

        $this->entityManager->flush();
    }
}