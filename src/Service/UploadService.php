<?php

namespace App\Service;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\String\Slugger\SluggerInterface;
use Symfony\Component\HttpFoundation\File\File;

/**
 * Class UploadService
 * @package App\Service
 */
class UploadService
{
    /**
     * @var string
     */
    private $storageDirectory;

    /**
     * @var SluggerInterface
     */
    private $slugger;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var PeopleService
     */
    private $peopleService;

    /**
     * @var ShipOrderService
     */
    private $shipOrderService;

    /**
     * UploadService constructor.
     * @param $storageDirectory
     * @param SluggerInterface $slugger
     * @param EntityManagerInterface $entityManager
     * @param PeopleService $peopleService
     * @param ShipOrderService $shipOrderService
     */
    public function __construct(
        $storageDirectory,
        SluggerInterface $slugger,
        EntityManagerInterface $entityManager,
        PeopleService $peopleService,
        ShipOrderService $shipOrderService
    ) {
        $this->storageDirectory = $storageDirectory;
        $this->slugger = $slugger;
        $this->entityManager = $entityManager;
        $this->peopleService = $peopleService;
        $this->shipOrderService = $shipOrderService;
    }

    /**
     * @param array $files
     */
    public function process(array $files): void
    {
        /** @var UploadedFile $file */
        foreach ($files['people'] as $file) {
            $newFile = $this->upload($file);
            $this->peopleService->persist($newFile->getContent());
        }

        /** @var UploadedFile $file */
        foreach ($files['shipOrders'] as $file) {
            $newFile = $this->upload($file);
            $this->shipOrderService->persist($newFile->getContent());
        }
    }

    /**
     * @param UploadedFile $file
     * @return File
     */
    private function upload(UploadedFile $file): File
    {
        $originalFilename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
        $safeFilename = $this->slugger->slug($originalFilename);
        $fileName = $safeFilename . '-' . uniqid() . '.' . $file->guessExtension();

        return $file->move($this->storageDirectory, $fileName);
    }
}