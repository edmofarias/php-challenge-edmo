<?php

namespace App\Service;

use App\Converter\PeopleConverter;
use Doctrine\ORM\EntityManagerInterface;
use Spatie\Async\Pool;

/**
 * Class PeopleService
 * @package App\Service
 */
class PeopleService
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var PeopleConverter
     */
    private $peopleConverter;

    /**
     * PeopleService constructor.
     * @param EntityManagerInterface $entityManager
     * @param PeopleConverter $peopleConverter
     */
    public function __construct(EntityManagerInterface $entityManager, PeopleConverter $peopleConverter)
    {
        $this->entityManager = $entityManager;
        $this->peopleConverter = $peopleConverter;
    }

    /**
     * @param string $peopleXml
     */
    public function persist(string $peopleXml)
    {
        $xmlObject = simplexml_load_string($peopleXml);

        $pool = Pool::create();

        foreach ($xmlObject->children() as $person) {
            $pool[] = async(function () use ($person) {
                $personEntity = $this->peopleConverter->convertXmlToObject($person);
                $this->entityManager->persist($personEntity);
            });
        }

        $pool->wait();

        $this->entityManager->flush();
    }
}