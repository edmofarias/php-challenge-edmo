<?php

namespace App\Tests\Converter;

use App\Converter\ShipOrderConverter;
use App\Entity\Person;
use App\Entity\ShipOrder;
use App\Repository\PersonRepository;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 * Class ShipOrderConvertTest
 * @package App\Tests\Service
 */
final class ShipOrderConvertTest extends KernelTestCase
{
    public function testConvertXmlShipOrderToObject()
    {
        $xmlFilePath = realpath(__DIR__ . '/../Assets/shiporders.xml');
        $xmlObject = simplexml_load_file($xmlFilePath);

        foreach ($xmlObject->children() as $shipOrder) {

            $personRepository = $this->getMockBuilder(PersonRepository::class)
                ->disableOriginalConstructor()
                ->setMethods(['find'])
                ->getMock();

            $personRepository->method('find')
                ->with($this->equalTo(intval($shipOrder->orderperson)))
                ->willReturn(new Person('Edmo Costa', ['77777998', '111222333'], intval($shipOrder->orderperson)))
            ;

            $shipOrderEntity = (new ShipOrderConverter($personRepository))->convertXmlToObject($shipOrder);
            $this->assertInstanceOf(ShipOrder::class, $shipOrderEntity);
        }
    }
}
