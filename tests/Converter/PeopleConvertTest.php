<?php

namespace App\Tests\Converter;

use App\Converter\PeopleConverter;
use App\Entity\Person;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 * Class PeopleConvertTest
 * @package App\Tests\Service
 */
final class PeopleConvertTest extends KernelTestCase
{
    public function testConvertXmlPeopleToObject()
    {
        $xmlFilePath = realpath(__DIR__ . '/../Assets/people.xml');
        $xmlObject = simplexml_load_file($xmlFilePath);

        foreach ($xmlObject->children() as $person) {
            $personEntity = (new PeopleConverter())->convertXmlToObject($person);
            $this->assertInstanceOf(Person::class, $personEntity);
        }
    }
}
