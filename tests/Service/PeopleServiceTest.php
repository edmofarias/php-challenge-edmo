<?php

namespace App\Tests\Converter;

use App\Converter\PeopleConverter;
use App\Entity\Person;
use App\Service\PeopleService;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 * Class PeopleServiceTest
 * @package App\Tests\Converter
 */
final class PeopleServiceTest extends KernelTestCase
{
    /**
     * @var \Doctrine\Bundle\DoctrineBundle\Registry|object|null
     */
    private $doctrine;

    protected function setUp()
    {
        $kernel = self::bootKernel();
        $this->doctrine = $kernel->getContainer()->get('doctrine');

        $this->doctrine->getConnection()->beginTransaction();
    }

    protected function tearDown(): void
    {
        $this->doctrine->getConnection()->rollBack();
        parent::tearDown();
    }

    public function testAddPeople()
    {
        $peopleConverter = new PeopleConverter($this->doctrine->getManager());
        $service = new PeopleService($this->doctrine->getManager(), $peopleConverter);

        $xmlFilePath = realpath(__DIR__ . '/../Assets/people.xml');
        $xmlObject = simplexml_load_file($xmlFilePath);

        $service->persist($xmlObject->asXML());

        $person10 = $this->doctrine->getManager()->getRepository(Person::class)->find(10);
        $this->assertInstanceOf(Person::class, $person10);

        $person20 = $this->doctrine->getManager()->getRepository(Person::class)->find(20);
        $this->assertInstanceOf(Person::class, $person20);

        $person30 = $this->doctrine->getManager()->getRepository(Person::class)->find(30);
        $this->assertInstanceOf(Person::class, $person30);
    }
}
