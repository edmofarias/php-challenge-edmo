# Invillia PHP Challenge
Test project for Invillia company

### The challenge
Your customer receives two XML models from his partner. This information must be
available for both web system and mobile app.

#### Requirement

 - [docker](https://docs.docker.com/engine/install/ubuntu/)
 - [docker-compose](https://docs.docker.com/compose/install/)

#### Installation

Clone this repository:

```bash
git clone git@gitlab.com:edmofarias/php-challenge-edmo.git; cd php-challenge-edmo;
```

And run the following commands:

The project has a docker-compose.yml file, you just need to run the command bellow and it will create tree containers: Nginx, Php, Mysql.
```bash
docker-compose up -d
```

``Wait a few minutes.``

To the project to work we need to bring the project dependencies. Access the PHP container and run composer install.
```bash
docker-compose exec php sh
```
```bash
composer install
```

Still in container php we need create the database schema run the doctrine create schema command.
```bash
bin/console doctrine:schema:create
```

after that, run chmod permission in the storage directory

```bash
chmod 777 storage/
```

### Instructions
  - Now just access our application: [http://localhost:8000/upload](http://localhost:8000/upload)
  - Access the upload screen /upload and submit the two files at the sametime, the 'happy way' is working
  - The files uploaded are in storage/

### Endpoints
  - You may see those routes documented in [http://localhost:8000/api/doc](http://localhost:8000/api/documentation)
    - /upload
    - /api/people
    - /api/person/{id}
    - /api/ship-orders
    - /api/ship-order/{id}

### Tests
To run the tests just enter on the PHP container and execute the command:
```bash
./bin/phpunit
```